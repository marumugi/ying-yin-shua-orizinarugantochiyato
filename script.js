var sheet = SpreadsheetApp.getActiveSpreadsheet().getActiveSheet();
var dateInformation = "";

// スケジュール領域の開始位置と初期値
var countColumn = 11;
var countColumnDef = 11;

// 開始予定日の入力位置
var startDateInputCol = 7;
var startDateInputRow = 5;

// 終了予定日の入力位置
var endDateInputCol = 9;
var endDateInputRow = 5;

// 開始実績日の入力位置
var acStartDateInputCol = 8;
var acStartDateInputRow = 5;

// 終了実績日の入力位置
var acEndDateInputCol = 10;
var acEndDateInputRow = 5;

// プロジェクト期間（月数）を取得
var periodProject　= sheet.getRange("I2").getValue();

// 予定日の描画背景色の指定
var inputBackgroundColor = ['#0F0', '#ccf','#ff3','#f3f','#f63'];

function scheduleUpdateButton() {
  scheduleUpdate();
}

function onEdit(param) {
  var range = param.range;
  
  // ========================== 日付表示の切り替え処理 Start ==========================
  
  // プロジェクト日付が変更されたとき
  if(range.getRow() == 1 && range.getColumn() == 9) {
    
    // スケジュール描画関数の実行
    scheduleUpdate();
  }
  
  // ========================== 日付表示の切り替え処理 End ==========================
  
  
  
  
  
  // ========================== 予定入力処理 Start ==========================
  
  
  // 開始/予定入力処理
  if(range.getColumn() == startDateInputCol && startDateInputRow < range.getRow()) {
    var editCellRow = range.getRow(); // 編集行取得
    var startDate = formatDate(range.getValue());
    startDate = startDate.split("/");
    
    // タスク初期化
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).setBackground("#FFF");
    sheet.getRange(editCellRow, 11, 1, 267).setBackground("#FFF");
    
    // ガントチャートの描画
    createSchedule("startYotei", editCellRow, startDate);
  }
  else if(range.getColumn() == endDateInputCol && endDateInputRow < range.getRow()) {
    // 終了/予定入力処理
    var editCellRow = range.getRow(); // 編集行取得
    var endDate = formatDate(range.getValue());
    var startDate = formatDate(sheet.getRange(editCellRow, startDateInputCol).getValue()); // 編集行の開始予定日の取得
    
    // 開始予定日付と終了予定日付の差分日数を取得
    var date1 = new Date(endDate);
    var date2 = new Date(startDate);
    var dateDiff = date1.getTime() - date2.getTime();
    dateDiff = Math.floor(dateDiff / (1000 * 60 * 60 *24));
    dateDiff++; // 差分日数
    
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).setBackground("#FFF");
      
    startDate = startDate.split("/");
    endDate = endDate.split("/");
    
    // ガントチャートの描画
    createSchedule("endYotei", editCellRow, startDate, endDate, dateDiff);
  }
  
  
  // ========================== 予定入力処理 End ==========================
  
  
  
  // ========================== 実績入力処理 Start ==========================
  
  // 開始/予定入力処理
  if(range.getColumn() == acStartDateInputCol && acStartDateInputRow < range.getRow()) {
    
    var editCellRow = range.getRow(); // 編集行取得
    var endDate = formatDate(sheet.getRange(editCellRow, endDateInputCol).getValue()); // 編集業の終了予定日の取得
    var startDate = formatDate(sheet.getRange(editCellRow, startDateInputCol).getValue()); // 編集行の開始予定日の取得
    var acStartDate = formatDate(range.getValue());    
    
    // 開始予定日付と終了予定日付の差分日数を取得
    var date1 = new Date(endDate);
    var date2 = new Date(startDate);
    var dateDiff = date1.getTime() - date2.getTime();
    dateDiff = Math.floor(dateDiff / (1000 * 60 * 60 *24));
    dateDiff++; // 差分日数
    
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).setBackground("#FFF");
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).clear();
    
    startDate = startDate.split("/");
    endDate = endDate.split("/");
    acStartDate = acStartDate.split("/");
      
    // ガントチャートの描画
    createSchedule("startJisseki", editCellRow, startDate, endDate, dateDiff, acStartDate);
  }
  else if(range.getColumn() == acEndDateInputCol && acEndDateInputRow < range.getRow()) {
    
    var editCellRow = range.getRow(); // 編集行取得
    var endDate = formatDate(sheet.getRange(editCellRow, endDateInputCol).getValue()); // 編集業の終了予定日の取得
    var startDate = formatDate(sheet.getRange(editCellRow, startDateInputCol).getValue()); // 編集行の開始予定日の取得
    var acStartDate = formatDate(sheet.getRange(editCellRow, acStartDateInputCol).getValue());
    var acEndDate = formatDate(range.getValue());
    
    // 開始予定日付と終了予定日付の差分日数を取得
    var date1 = new Date(endDate);
    var date2 = new Date(startDate);
    var dateDiff = date1.getTime() - date2.getTime();
    dateDiff = Math.floor(dateDiff / (1000 * 60 * 60 *24));
    dateDiff++; // 差分日数
    
    date1 = new Date(acEndDate);
    date2 = new Date(acStartDate);
    var acDateDiff = date1.getTime() - date2.getTime();
    acDateDiff = Math.floor(acDateDiff / (1000 * 60 * 60 *24));
    acDateDiff++; // 差分日数
    
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).setBackground("#FFF");
    sheet.getRange("K" + editCellRow + ":JQ" + editCellRow).clear();
    
    startDate = startDate.split("/");
    endDate = endDate.split("/");
    acStartDate = acStartDate.split("/");
    acEndDate = acEndDate.split("/");
      
    // ガントチャートの描画
    createSchedule("endJisseki", editCellRow, startDate, endDate, dateDiff, acStartDate, acEndDate, acDateDiff);
  }
  
  // ========================== 実績入力処理 End ==========================
}





// 日付フォーマット関数
function formatDate(dateValue) {
  return Utilities.formatDate(dateValue, "JST", "yyyy/M/d");
}




// 差分日数を求める関数
function getDateDifference(endDateArg, startDateArg, dateDiffArg, flg) {
  var returnValue;
  var date1 = new Date(endDateArg);
  var date2 = new Date(startDateArg);
  var date3 = date1.getTime() - date2.getTime();
  date3 = Math.floor(date3 / (1000 * 60 * 60 *24));
  
  // フラグが立っていた場合は元の差分日数から減算する
  if(flg) {
    returnValue = dateDiffArg - date3;
  }
  else {
    // フラグが立っていない場合は差分日数を保存する
    returnValue = date3 + 1;
  }
  
  return returnValue;
}





// スケジュール情報の保存
function saveSchedule(year, month) {
  
  var scheduleDate = "";
  var schedule = {};
  // 入力月からプロジェクト期間終了月の月末日をオブジェクトへ格納
  for(i = 0; i < periodProject; i++) {
    scheduleDate = new Date(year, month, 0);
    scheduleDate = formatDate(scheduleDate);
    scheduleDate = scheduleDate.split("/");
    
    schedule[i] = {
      month: month,
      year: year,
      lastMonth: scheduleDate[2]
    };
    
    month++;
    if(12 < month) {
      month = 1;
      year++;
    }
  }
  return schedule;
}





// スケジュール情報の描画処理
function scheduleUpdate() {
  
  // 日付表示のクリア
  sheet.getRange(1,11,1,sheet.getLastColumn()).clearContent();
  sheet.getRange(2,11,1,sheet.getLastColumn()).clearContent();
  sheet.getRange(3,11,1,sheet.getLastColumn()).clearContent();
  sheet.getRange("K3:JQ3").setBackground("#FFF");
  
  // 入力された日付を取得する
    var targetDate = sheet.getRange(1, 9).getValue();
    targetDate = formatDate(targetDate);
    
    // 入力月を取得
    var targetMonth = targetDate.split("/");
    dateInformation = saveSchedule(targetMonth[0], targetMonth[1]);
    
    // 日付表記セルの列幅調整
    var cnt = 0;
    var weekDayList = ["日", "月", "火", "水", "木", "金", "土"];
    var saturdayColor = "#9BFFA0";
    var sundayColor = "#FF9B9B";
    
    for(i = 0; i < Object.keys(dateInformation).length; i++) {
      for(j = 1; j <= dateInformation[i]["lastMonth"]; j++) {
        
        // 最初の月のループ
        if(i == 0){
          
          // 月をセット
          if(j == 1) {
            sheet.getRange(1, countColumn).setValue(dateInformation[i]["month"] + "月");
          }
          
          // 日数をセット
          if(j >= targetMonth[2]) {
            sheet.getRange(2, countColumn).setValue(j);
            sheet.setColumnWidth(countColumn, 30);
            
            // 曜日のセット
            var setDay = new Date(dateInformation[i]["year"] + "/" + dateInformation[i]["month"] + "/" + j);
            sheet.getRange(3, countColumn).setValue(weekDayList[setDay.getDay()]);
            
            // 土日の色変更
            if(setDay.getDay() == 6) {
              sheet.getRange(3, countColumn).setBackground(saturdayColor);
            }
            else if(setDay.getDay() == 0) {
              sheet.getRange(3, countColumn).setBackground(sundayColor);
            }
            
            countColumn++;
          }
        }
        else {
          if(j == 1) {
            sheet.getRange(1, countColumn).setValue(dateInformation[i]["month"] + "月");
          }
          
          // その他月のループ
          sheet.getRange(2, countColumn).setValue(j);
          sheet.setColumnWidth(countColumn, 30);
          
          // 曜日のセット
          var setDay = new Date(dateInformation[i]["year"] + "/" + dateInformation[i]["month"] + "/" + j);
          sheet.getRange(3, countColumn).setValue(weekDayList[setDay.getDay()]);
          
          // 土日の色変更
          if(setDay.getDay() == 6) {
            sheet.getRange(3, countColumn).setBackground(saturdayColor);
          }
          else if(setDay.getDay() == 0) {
            sheet.getRange(3, countColumn).setBackground(sundayColor);
          }
          
          countColumn++;
        }
      }
    }
    countColumn = countColumnDef;
    
    
    // 各スケジュール情報の再描画
    
    sheet.getRange("K" + 6 + ":JQ" + sheet.getLastRow()).setBackground("#FFF");
  sheet.getRange("K" + 6 + ":JQ" + sheet.getLastRow()).clear();
    
    // ガントチャート描画処理
    createSchedule("all", "", "", "", "");
}




// ガントチャートの作成関数
function createSchedule(type, editCellRow, startDate, endDate, dateDiff, acStartDate, acEndDate, acDateDiff) { 
  // ループ変数
  var loopDefaultValue;
  var loopMax;
  
  // スケジュール開始日付を取得
  var targetDate = sheet.getRange(1, 9).getValue();
  targetDate = formatDate(targetDate);
  var targetMonth = targetDate.split("/");
  
  // スケジュール情報を取得
  dateInformation = saveSchedule(targetMonth[0], targetMonth[1]);
  var countFlg = 0;
  var acCountFlg = 0;
  
  // 実績差分日数の保持変数
  var acDiffDef;
  
  if(type == "endJisseki") {
    acDiffDef = acDateDiff;
  }
  
  // スケジュール情報再作成時の処理
  if(type == "all") {
    loopDefaultValue = 6;
    loopMax = sheet.getLastRow();
  }
  else {
    loopDefaultValue = 1;
    loopMax = 1;
  }
  
  for(x = loopDefaultValue; x <= loopMax; x++) {
    
    // スケジュール再作成時
    if(type == "all") {
      if(sheet.getRange(x, startDateInputCol).getValue() != "" && sheet.getRange(x, endDateInputCol).getValue() != "") {
        startDate = formatDate(sheet.getRange(x, startDateInputCol).getValue());
        endDate = formatDate(sheet.getRange(x, endDateInputCol).getValue());
        
        // 開始予定日付と終了予定日付の差分日数を取得
        dateDiff = getDateDifference(endDate, startDate, "", 0);
        
        startDate = startDate.split("/");
        endDate = endDate.split("/");
        
        editCellRow = x;
      }
      
      if(sheet.getRange(x, acStartDateInputCol).getValue() != "" && sheet.getRange(x, acEndDateInputCol).getValue() != "") {
        acStartDate = formatDate(sheet.getRange(x, acStartDateInputCol).getValue());
        acEndDate = formatDate(sheet.getRange(x, acEndDateInputCol).getValue());
        
        // 開始実績日付と終了実績日付の差分日数を取得
        acDateDiff = getDateDifference(acEndDate, acStartDate, "", 0);
        
        acStartDate = acStartDate.split("/");
        acEndDate = acEndDate.split("/");
        acDiffDef = acDateDiff;
      }
      else {
        acStartDate = "";
        acEndDate = "";
      }
    }
  
    // 予定日付の作成
    for(i = 0; i < Object.keys(dateInformation).length; i++) {
      for(j = 1; j <= dateInformation[i]["lastMonth"]; j++) {
        if(i == 0) {
          if(j >= targetMonth[2]) {
            
            // 開始予定日入力時の処理
            if(type == "startYotei") { 
              if(dateInformation[i]["year"] == startDate[0] && dateInformation[i]["month"] == startDate[1] && j == startDate[2]) {
                sheet.getRange(editCellRow, countColumn).setBackground("#f00");
              }
            }
            
            // 終了予定日入力時の処理
            if(type == "endYotei" || type == "startJisseki" || type == "endJisseki") {
              // 開始予定日から終了予定日のセルを塗りつぶす
              // 開始予定日が来たらフラグを立てる
              
              // 開始予定日より終了予定日の方が後の場合
              if(dateInformation[i]["year"] == startDate[0] && startDate[1] == dateInformation[i]["month"]) {
                if(startDate[2] == j) {
                  countFlg = 1;
                }
              }
            }
            
            
            // スケジュール情報再作成時の処理
            if(type == "all") {
              if(dateDiff != 0) {
                
                if(startDate[0] == dateInformation[i]["year"]) {
                  if(startDate[1] == dateInformation[i]["month"]) {
                    if(startDate[2] <= j) {
                      // 開始予定日付がスケジュールの表示日付より以前であり、終了予定日付がスケジュールの表示領域無いだった場合
                      // 開始予定日付とスケジュールの表示日付（開始時点の）の差分をdateDiff変数から減算する
                      if(countFlg == 0 && startDate[2] != j) {
                        dateDiff = getDateDifference(dateInformation[i]["year"]+"/"+dateInformation[i]["month"]+"/"+j, startDate[0]+"/"+startDate[1]+"/"+startDate[2], dateDiff, 1);
                      }
                      
                      // 減算結果が0より低くなったらフラグを0にする
                      if(dateDiff <= 0) {
                        countFlg = 0;
                      }
                      else {
                        countFlg = 1;
                      }
                      
                    }
                    else {
                      countFlg = 0;
                    }
                  }
                  else if(parseInt(startDate[1]) < parseInt(dateInformation[i]["month"])) {
                    if(countFlg == 0) {
                      var scheduleStart = formatDate(sheet.getRange(1, 9).getValue());// スケジュール描画開始日
                      dateDiff = getDateDifference(scheduleStart, startDate[0]+"/"+startDate[1]+"/"+startDate[2], dateDiff, 1);
                      
                      // 減算結果が0より低くなったらフラグを0にする
                      if(dateDiff <= 0) {
                        countFlg = 0;
                      }
                      else {
                        countFlg = 1;
                      }
                    }
                  }
                }
                else if(startDate[0] < dateInformation[i]["year"]) {
                  if(countFlg == 0) {
                    var scheduleStart = formatDate(sheet.getRange(1, 9).getValue());// スケジュール描画開始日
                    dateDiff = getDateDifference(scheduleStart, startDate[0]+"/"+startDate[1]+"/"+startDate[2], dateDiff, 1);
                    
                    // 減算結果が0より低くなったらフラグを0にする
                    if(dateDiff <= 0) {
                      countFlg = 0;
                    }
                    else {
                      countFlg = 1;
                    }
                  }
                }
                
                
              }
              
              if(acDateDiff != 0) {
                
                if(acStartDate[0] == dateInformation[i]["year"]) {
                  if(acStartDate[1] == dateInformation[i]["month"]) {
                    if(acStartDate[2] <= j) {
                      // 開始実績日付がスケジュールの表示日付より以前であり、終了実績日付がスケジュールの表示領域内だった場合
                      // 開始実績日付とスケジュールの表示日付（開始時点の）の差分をdateDiff変数から減算する
                      if(acCountFlg == 0 && acStartDate[2] != j) {
                        acDateDiff = getDateDifference(dateInformation[i]["year"]+"/"+dateInformation[i]["month"]+"/"+j, acStartDate[0]+"/"+acStartDate[1]+"/"+acStartDate[2], acDateDiff, 1);
                        
                      }
                      
                      // 減算結果が0より低くなったらフラグを0にする
                      if(acDateDiff <= 0) {
                        acCountFlg = 0;
                      }
                      else {
                        acCountFlg = 1;
                      }
                    }
                    else {
                      acCountFlg = 0;
                    }
                  }
                  else if(parseInt(acStartDate[1]) < parseInt(dateInformation[i]["month"])) {
                    if(acCountFlg == 0) {
                      var scheduleStart = formatDate(sheet.getRange(1, 9).getValue());// スケジュール描画開始日
                      acDateDiff = getDateDifference(scheduleStart, acStartDate[0]+"/"+acStartDate[1]+"/"+acStartDate[2], acDateDiff, 1);
                      
                      // 減算結果が0より低くなったらフラグを0にする
                      if(acDateDiff <= 0) {
                        acCountFlg = 0;
                      }
                      else {
                        acCountFlg = 1;
                      }
                    }
                  }
                }
                else if(acStartDate[0] < dateInformation[i]["year"]) {
                  // 表示中のスケジュール情報の年より前の年から開始日が指定されていた場合の処理
                  if(acCountFlg == 0) {
                    var scheduleStart = formatDate(sheet.getRange(1, 9).getValue());// スケジュール描画開始日
                    acDateDiff = getDateDifference(scheduleStart, acStartDate[0]+"/"+acStartDate[1]+"/"+acStartDate[2], acDateDiff, 1);
                    
                    // 減算結果が0より低くなったらフラグを0にする
                    if(acDateDiff <= 0) {
                      acCountFlg = 0;
                    }
                    else {
                      acCountFlg = 1;
                    }
                  }
                }
                
                
              }
            }
            
            if(type == "endYotei" || type == "all" || type == "startJisseki" || type == "endJisseki") {
              var typeEditCellRowColor = editCellRow % 5; //　編集行ごとに背景色のタイプを指定
              
              // フラグが立っている時は背景色を変更する
              if(countFlg == 1) {
                sheet.getRange(editCellRow, countColumn).setBackground(inputBackgroundColor[typeEditCellRowColor]);
                
                // 1セル塗りつぶしたら差分日数をマイナス1する
                dateDiff--;
              }
              
              // 全ての差分日数を塗りつぶしたらフラグを初期化
              if(dateDiff == 0) {
                countFlg = 0;
              }
            }
            
            // 実績開始日を入力した際、開始記号を入力する
            if(type == "startJisseki" || type == "endJisseki") {
              if(dateInformation[i]["year"] == acStartDate[0] && acStartDate[1] == dateInformation[i]["month"] && acStartDate[2] == j) {
                sheet.getRange(editCellRow, countColumn).setValue("→");
              }
            }
            
            // 実績表示処理
            // 終了実績日入力時の処理
            if(type == "endJisseki" || type == "all") {
              // 開始予定日から終了予定日のセルを塗りつぶす
              // 開始予定日が来たらフラグを立てる
              if(acStartDate[0] == dateInformation[i]["year"] && acStartDate[1] == dateInformation[i]["month"]) {
                if(acStartDate[2] == j) {
                  acCountFlg = 1;
                }
              }
              
              // フラグが立っている時は「─」を入力する
              if(acCountFlg == 1) {
                
                if(acDiffDef == acDateDiff) {
                  sheet.getRange(editCellRow, countColumn).setValue("→");
                }
                else if(acDateDiff == 1) {
                  sheet.getRange(editCellRow, countColumn).setValue("●");
                }
                else {
                  sheet.getRange(editCellRow, countColumn).setValue("─");
                }
                
                // 1セルに入力したら差分日数をマイナス1する
                acDateDiff--;
              }
              
              // 全ての差分日数を入力したらフラグを初期化
              if(acDateDiff == 0) {
                acCountFlg = 0;
              }
            }
            
            countColumn++;
          }
        }
        else {
          
          // 開始予定日入力時の処理
          if(type == "startYotei") {
            if(dateInformation[i]["year"] == startDate[0] && dateInformation[i]["month"] == startDate[1] && j == startDate[2]) {
              sheet.getRange(editCellRow, countColumn).setBackground("#F00");
            }
          }
          
          // 終了予定日入力時の処理
          if(type == "endYotei" || type == "startJisseki" || type == "endJisseki") {
            // 開始予定日から終了予定日のセルを塗りつぶす
            // 開始予定日が来たらフラグを立てる
            if(dateInformation[i]["year"] == startDate[0] && startDate[1] == dateInformation[i]["month"]) {
              if(startDate[2] == j) {
                countFlg = 1;
              }
            }
          }
            
          // スケジュール情報再作成時の処理
          if(type == "all") {
            if(dateDiff != 0) {
              if(startDate[0] == dateInformation[i]["year"] && startDate[1] == dateInformation[i]["month"]) {
                if(startDate[2] <= j) {
                  countFlg = 1;
                }
                else {
                  countFlg = 0;
                }
              }
            }
            
            if(acDateDiff != 0) {
              if(acStartDate[0] == dateInformation[i]["year"] && acStartDate[1] == dateInformation[i]["month"]) {
                if(acStartDate[2] <= j) {
                  acCountFlg = 1;
                }
                else {
                  acCountFlg = 0;
                }
              }
            }
          }
          
          if(type == "endYotei" || type == "all" || type == "startJisseki" || type == "endJisseki") {
            // フラグが立っている時は背景色を変更する
            //if(countFlg == 1) {
            //  sheet.getRange(editCellRow, countColumn).setBackground("#0F0");
            if(countFlg == 1) {
              sheet.getRange(editCellRow, countColumn).setBackground(inputBackgroundColor[typeEditCellRowColor]);
              
              // 1セル塗りつぶしたら差分日数をマイナス1する
              dateDiff--;
            }
            
            // 全ての差分日数を塗りつぶしたらフラグを初期化
            if(dateDiff == 0) {
              countFlg = 0;
            }
          }
          
          // 実績開始日を入力した際、開始記号を入力する
          if(type == "startJisseki" || type == "endJisseki") {
            if(dateInformation[i]["year"] == acStartDate[0] && acStartDate[1] == dateInformation[i]["month"] && acStartDate[2] == j) {
              sheet.getRange(editCellRow, countColumn).setValue("→");
            }
          }
          
          // 実績表示処理
          // 終了実績日入力時の処理
          if(type == "endJisseki" || type == "all") {
            // 開始予定日から終了予定日のセルを塗りつぶす
            // 開始予定日が来たらフラグを立てる
            if(acStartDate[0] == dateInformation[i]["year"] && acStartDate[1] == dateInformation[i]["month"]) {
              if(acStartDate[2] == j) {
                acCountFlg = 1;
              }
            }
            
            // フラグが立っている時は「─」を入力する
            if(acCountFlg == 1) {
              
              if(acDiffDef == acDateDiff) {
                sheet.getRange(editCellRow, countColumn).setValue("→");
              }
              else if(acDateDiff == 1) {
                sheet.getRange(editCellRow, countColumn).setValue("●");
              }
              else {
                sheet.getRange(editCellRow, countColumn).setValue("─");
              }
              
              // 1セルに入力したら差分日数をマイナス1する
              acDateDiff--;
            }
            
            // 全ての差分日数を入力したらフラグを初期化
            if(acDateDiff == 0) {
              acCountFlg = 0;
            }
          }
          
          countColumn++;
        }
      }
    }
    countFlg = 0;
    acCountFlg = 0;
    countColumn = countColumnDef;
  }
}